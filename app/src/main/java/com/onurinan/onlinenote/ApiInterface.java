package com.onurinan.onlinenote;

import com.onurinan.onlinenote.Response.LoginItem;
import com.onurinan.onlinenote.Response.MyTextsResponse;
import com.onurinan.onlinenote.Response.RegisterItem;
import com.onurinan.onlinenote.Response.TextItem;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("Login/Login")
    Call<LoginItem> getSelectedLogin(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("Login/UpsertRegister")
    Call<RegisterItem> setSelectedRegister(@Field("userID") Integer userID,
                                           @Field("userName") String userName,
                                           @Field("userSurname") String userSurname,
                                           @Field("email") String email,
                                           @Field("password") String password);

    @FormUrlEncoded
    @POST("Login/UpsertText")
    Call<TextItem> setSelectedText(@Field("textID") Integer textID,
                                   @Field("userID") Integer userID,
                                   @Field("textContent") String textContent,
                                   @Field("ownerID") Integer ownerID,
                                   @Field("size") Integer size,
                                   @Field("background") String background);

    @FormUrlEncoded
    @POST("Login/GetMyTexts")
    Call<MyTextsResponse> getSelectedMyText(@Field("userID") Integer userID);

    @FormUrlEncoded
    @POST("Login/TextDelete")
    Call<TextItem> deleteText(@Field("textID") Integer textID);

    @FormUrlEncoded
    @POST("Login/GetText")
    Call<MyTextsResponse> getText(@Field("guid") String guid);

}
