package com.onurinan.onlinenote;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.onurinan.onlinenote.Response.LoginItem;
import com.onurinan.onlinenote.utils.SharedPrefUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    ApiInterface apiservis = ApiClient.getClient().create(ApiInterface.class);
    private EditText email;
    private EditText password;
    private Button startnButton;
    private ProgressDialog progressDialog;
    private TextView registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.editTextemail);
        password = findViewById(R.id.edittextpassword);
        startnButton = findViewById(R.id.startButton);
        registerButton=findViewById(R.id.TextViewRegisterButton);


        startnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpaty(email) && isEmpaty(password)) {
                    getSelectedLogin(email.getText().toString(),password.getText().toString());
                } else if (!isEmpaty(email)) {
                    email.requestFocus();
                    email.setError("Lütfen email adresinizi giriniz");
                } else {
                    password.requestFocus();
                    password.setError("Lütfen şifrenizi giriniz");
                }
            }
        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    private boolean isEmpaty(EditText editText) {
        if (editText.getText().toString().trim().length() > 0) {
            return true;
        } else
            return false;
    }

    public void getSelectedLogin(String email, String password) {
        Call<LoginItem> call = apiservis.getSelectedLogin(email, password);
        call.enqueue(new Callback<LoginItem>() {
            @Override
            public void onResponse(Call<LoginItem> call, Response<LoginItem> response) {

                LoginItem loginItem = response.body();
                if (loginItem.getUserName()==null)
                {
                    Toast.makeText(getApplicationContext(), "Böyle bir kullancı bulunmamaktadır!!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Kullanıcı girişi başarılı", Toast.LENGTH_LONG).show();
                    SharedPrefUtil.setInt("userID", loginItem.getUserID(), LoginActivity.this);
                    SharedPrefUtil.setString("userName", loginItem.getUserName(), LoginActivity.this);
                    SharedPrefUtil.setString("userSurname", loginItem.getUserSurname(), LoginActivity.this);
                    Intent intent = new Intent(LoginActivity.this, NoteListActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<LoginItem> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "Beklenmeyen bir hata oluşmaktadır!!", Toast.LENGTH_LONG).show();

            }
        });
    }


}