package com.onurinan.onlinenote.Response;

public class RegisterItem {
    private String email;
    private String password;
    private Integer userID;
    private String userName;
    private String userSurname;

    public RegisterItem(Integer num, String str, String str2, String str3, String str4) {
        this.userID = num;
        this.userName = str;
        this.userSurname = str2;
        this.email = str3;
        this.password = str4;
    }

    public Integer getUserID() {
        return this.userID;
    }

    public void setUserID(Integer num) {
        this.userID = num;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String str) {
        this.userName = str;
    }

    public String getUserSurname() {
        return this.userSurname;
    }

    public void setUserSurname(String str) {
        this.userSurname = str;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String str) {
        this.email = str;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String str) {
        this.password = str;
    }
}
