package com.onurinan.onlinenote.Response;

public class LoginItem {
    private String message;
    private boolean sucsess;
    private int userID;
    private String userName;
    private String userSurname;

    public LoginItem(int i, String str, String str2, boolean z, String str3) {
        this.userID = i;
        this.userName = str;
        this.userSurname = str2;
        this.sucsess = z;
        this.message = str3;
    }

    public int getUserID() {
        return this.userID;
    }

    public void setUserID(int i) {
        this.userID = i;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String str) {
        this.userName = str;
    }

    public String getUserSurname() {
        return this.userSurname;
    }

    public void setUserSurname(String str) {
        this.userSurname = str;
    }

    public boolean isSucsess() {
        return this.sucsess;
    }

    public void setSucsess(boolean z) {
        this.sucsess = z;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String str) {
        this.message = str;
    }
}
