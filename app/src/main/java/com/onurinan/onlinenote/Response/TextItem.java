package com.onurinan.onlinenote.Response;

import java.io.Serializable;
import java.util.Date;

public class TextItem implements Serializable {
    private String background;
    private int ownerID;
    private int size;
    private String textContent;
    private int textID;
    private int userID;

    private Date createDate;
    private String guid;

    public TextItem(int userID, int textID, String textContent, int size, String background) {
        this.userID = userID;
        this.textID = textID;
        this.textContent = textContent;
        this.size = size;
        this.background = background;
    }

    public String toString() {
        return "TextItem{textID=" + this.textID + ", userID=" + this.userID + ", ownerID=" + this.ownerID + ", textContent='" + this.textContent + '\'' + ", size=" + this.size + ", background='" + this.background + '\'' + '}';
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public int getTextID() {
        return textID;
    }

    public void setTextID(int textID) {
        this.textID = textID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }
}
