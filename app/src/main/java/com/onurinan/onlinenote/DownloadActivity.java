package com.onurinan.onlinenote;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.onurinan.onlinenote.Adapter.CustomAdapter;
import com.onurinan.onlinenote.Response.MyTextsResponse;
import com.onurinan.onlinenote.Response.TextItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownloadActivity extends AppCompatActivity {

    ApiInterface apiservis = ApiClient.getClient().create(ApiInterface.class);

    private EditText etCode;
    private Button btnShow;
    private TextView tvText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        etCode = findViewById(R.id.etCode);
        btnShow = findViewById(R.id.btnShow);
        tvText = findViewById(R.id.tvText);
        tvText.setMovementMethod(new ScrollingMovementMethod());

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShow();
            }
        });
    }

    private void onShow() {
        String code = etCode.getText().toString();
        if (code.length() == 0) {
            Toast.makeText(getApplicationContext(), "Lütfen kodu giriniz.", Toast.LENGTH_LONG).show();
            return;
        }

        Call<MyTextsResponse> listCall=apiservis.getText(code);
        listCall.enqueue(new Callback<MyTextsResponse>() {
            @Override
            public void onResponse(Call<MyTextsResponse> call, Response<MyTextsResponse> response) {
                MyTextsResponse myTextsResponse = response.body();
                if (myTextsResponse != null && myTextsResponse.sucsess) {
                    TextItem text = myTextsResponse.InfoTexts.get(0);
                    tvText.setText(text.getTextContent());
                } else {
                    Toast.makeText(getApplicationContext(), "Not bulunamadı!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MyTextsResponse> call, Throwable t) {

            }
        });
    }
}