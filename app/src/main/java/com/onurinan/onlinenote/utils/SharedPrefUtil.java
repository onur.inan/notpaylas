package com.onurinan.onlinenote.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefUtil {

    private static final String name = "kayit";

    public static int getInt(String key, Context c) {
        SharedPreferences sharedveriler = c.getSharedPreferences(name, Context.MODE_PRIVATE);
        return sharedveriler.getInt(key, 0);
    }

    public static void setInt(String key, int value, Context c) {
        SharedPreferences sharedveriler = c.getSharedPreferences(name, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedveriler.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void setString(String key, String value, Context c) {
        SharedPreferences sharedveriler = c.getSharedPreferences(name, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedveriler.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
