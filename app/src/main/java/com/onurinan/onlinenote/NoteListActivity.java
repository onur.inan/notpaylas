package com.onurinan.onlinenote;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.onurinan.onlinenote.Adapter.CustomAdapter;
import com.onurinan.onlinenote.Response.MyTextsResponse;
import com.onurinan.onlinenote.Response.TextItem;
import com.onurinan.onlinenote.utils.SharedPrefUtil;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoteListActivity extends AppCompatActivity {

    ApiInterface apiservis = ApiClient.getClient().create(ApiInterface.class);
    Integer userid;
    List<TextItem> listmytext = new ArrayList<>();
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);

        userid = SharedPrefUtil.getInt("userID", this);
        if (userid == 0) {
            showLogin();
            return;
        }

        listView = findViewById(R.id.lw_mynotelist);
        initListView();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getBaseContext(), AddNoteActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (userid > 0) {
            getAllMyNote();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notelist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_download) {
            onDownload();
        } else if (id == R.id.action_logout) {
            onLogout();
        }
        return super.onOptionsItemSelected(item);
    }

    private void onLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(NoteListActivity.this);
        builder.setTitle("Çıkış Yap");
        builder.setMessage("Çıkış yapmak istediğinizden emin misiniz?");
        builder.setNegativeButton("Hayır", null);
        builder.setPositiveButton("Evet", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SharedPrefUtil.setInt("userID", 0, getApplicationContext());
                showLogin();
            }
        });
        builder.show();
    }

    private void onDownload() {
        Intent intent = new Intent(NoteListActivity.this, DownloadActivity.class);
        startActivity(intent);
    }

    public void initListView() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextItem item = listmytext.get(position);
                Intent intent = new Intent(getApplicationContext(), AddNoteActivity.class);
                intent.putExtra("textItem", item);
                startActivity(intent);
            }
        });
    }

    public void showLogin() {
        Intent intent = new Intent(NoteListActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void getAllMyNote()
    {
        Call<MyTextsResponse> listCall=apiservis.getSelectedMyText(userid);
        listCall.enqueue(new Callback<MyTextsResponse>() {
            @Override
            public void onResponse(Call<MyTextsResponse> call, Response<MyTextsResponse> response) {
                listmytext = response.body().InfoTexts;
                CustomAdapter adapter = new CustomAdapter(NoteListActivity.this, listmytext);
                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<MyTextsResponse> call, Throwable t) {

            }
        });
    }
}