package com.onurinan.onlinenote;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.onurinan.onlinenote.Response.RegisterItem;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private EditText userName, userSurname, email, password, passwordAgain;
    private Button registerSave;
    ApiInterface apiservis = ApiClient.getClient().create(ApiInterface.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        userName = findViewById(R.id.editTextuserName);
        userSurname = findViewById(R.id.editTextuserSurname);
        email = findViewById(R.id.editTextTextemail);
        password = findViewById(R.id.edittextpassword1);
        passwordAgain = findViewById(R.id.edittextpasswordagain1);

        registerSave = findViewById(R.id.registerButton);

        registerSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSave();
            }
        });

    }

    private void onSave() {
        if (isNotEmpty(userName) && isNotEmpty(userSurname)
                && isNotEmpty(email) && isNotEmpty(password)
                && isNotEmpty(passwordAgain)
                && (password.getText().toString().equals(passwordAgain.getText().toString()))) {
            setRegisterUserDB(
                    userName.getText().toString()
                    , userSurname.getText().toString()
                    , email.getText().toString()
                    , password.getText().toString()
            );
            Toast.makeText(getApplicationContext(), "Kayit işmeli başarılı", Toast.LENGTH_LONG).show();

        } else if (!isNotEmpty(userName)) {
            userName.requestFocus();
            userName.setError("Ad alanı boş bıraklımaz");
        } else if (!isNotEmpty(userSurname)) {
            userSurname.requestFocus();
            userSurname.setError("Soyad alanı boş bıraklımaz");
        } else if (!isNotEmpty(email)) {
            email.requestFocus();
            email.setError("Email alanı boş bıraklımaz");
        } else if (!isNotEmpty(password)) {
            password.requestFocus();
            password.setError("Şifre alanı boş bıraklımaz");
        } else if (!isNotEmpty(passwordAgain) && (password.getText().toString() != passwordAgain.getText().toString())) {
            passwordAgain.requestFocus();
            passwordAgain.setError("Şifreler birbirleriyle örtüşmemektedir");
        }
    }

    private boolean isNotEmpty(EditText editText) {
        if (editText.getText().toString().trim().length() > 0) {
            return true;
        } else
            return false;
    }

    public void setRegisterUserDB(String username, String usersurname, String email, String password) {
        Call<RegisterItem> itemCall = apiservis.setSelectedRegister(0, username, usersurname, email, password);
        itemCall.enqueue(new Callback<RegisterItem>() {
            @Override
            public void onResponse(Call<RegisterItem> call, Response<RegisterItem> response) {
                Toast.makeText(getApplicationContext(), "Kayıt işlemi başarılı", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<RegisterItem> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Beklenmeyen bir hata oluşmaktadır!!", Toast.LENGTH_LONG).show();
            }
        });
    }


}