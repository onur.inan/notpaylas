package com.onurinan.onlinenote.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.onurinan.onlinenote.R;
import com.onurinan.onlinenote.Response.TextItem;

import java.util.List;

public class CustomAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<TextItem> myTextItemList;

    public long getItemId(int i) {
        return (long) i;
    }

    public CustomAdapter(Activity activity, List<TextItem> list) {
        this.inflater = LayoutInflater.from(activity);
        this.myTextItemList = list;
    }

    public int getCount() {
        return this.myTextItemList.size();
    }

    public Object getItem(int i) {
        return this.myTextItemList.get(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = inflater.inflate(R.layout.custom_layout, (ViewGroup) null);
        TextItem myTextItem = this.myTextItemList.get(i);
        ((TextView) inflate.findViewById(R.id.textContent)).setText(myTextItem.getTextContent());
     //   TextView textid=inflate.findViewById(R.id.txtıd);
     //   textid.setText(myTextItem.getTextID());
        return inflate;
    }
}
