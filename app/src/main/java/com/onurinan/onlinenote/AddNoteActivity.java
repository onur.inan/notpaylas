package com.onurinan.onlinenote;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.onurinan.onlinenote.Response.TextItem;
import com.onurinan.onlinenote.utils.SharedPrefUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNoteActivity extends AppCompatActivity {
    ApiInterface apiservis = ApiClient.getClient().create(ApiInterface.class);
    Integer userid;
    EditText textcontent;
    TextItem textItem;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        userid = SharedPrefUtil.getInt("userID", getApplicationContext());
        textcontent = findViewById(R.id.etTextContent);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            textItem = (TextItem) bundle.get("textItem");
            if (textItem != null) {
                textcontent.setText(textItem.getTextContent());
            }
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTextDB();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (textItem != null) {
            getMenuInflater().inflate(R.menu.addnote_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete) {
           onDelete();
        } else if (id == R.id.action_share) {
            onShare();
        }
        return super.onOptionsItemSelected(item);
    }

    private void onShare() {
        String guid = textItem.getGuid();

        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Not Paylaş", guid);
        clipboard.setPrimaryClip(clip);

        Toast.makeText(getApplicationContext(), "Kod panoya kapyalandı. Alıcıya gönderebilirsiniz.", Toast.LENGTH_LONG).show();
    }

    private void onDelete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddNoteActivity.this);
        builder.setTitle("Sil");
        builder.setMessage("Bu notu silmek istediğinizden emin misiniz?");
        builder.setNegativeButton("Hayır", null);
        builder.setPositiveButton("Evet", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Call<TextItem> itemCall = apiservis.deleteText(textItem.getTextID());
                itemCall.enqueue(new Callback<TextItem>() {
                    @Override
                    public void onResponse(Call<TextItem> call, Response<TextItem> response) {
                        Toast.makeText(getApplicationContext(), "Not başarılı bir şekilde silinmiştir", Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<TextItem> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Beklenmeyen bir hata oluşmaktadır!!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        builder.show();
    }

    private void setTextDB() {
        int textID = 0;
        if (textItem != null) {
            textID = textItem.getTextID();
        }

        String text = textcontent.getText().toString();
        if (text.length() == 0) {
            Toast.makeText(getApplicationContext(), "Lütfen notunuzu giriniz.", Toast.LENGTH_LONG).show();
            return;
        }

        Call<TextItem> itemCall = apiservis.setSelectedText(textID, userid, text, 0, 15, "background");
        itemCall.enqueue(new Callback<TextItem>() {
            @Override
            public void onResponse(Call<TextItem> call, Response<TextItem> response) {
                Toast.makeText(getApplicationContext(), "Notunuz başarılı bir şekilde oluşturulmuştur.", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<TextItem> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Beklenmeyen bir hata oluşmaktadır!!", Toast.LENGTH_LONG).show();
            }
        });
    }

}
